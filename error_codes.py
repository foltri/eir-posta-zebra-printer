codes = {
    '18': {
            "code": 18,
            "message": "Cutter Fault"
    },
    '14': {
            "code": 14,
            "message": "Head Open"
    },
    '12': {
            "code": 12,
            "message": "Ribbon Out"
    },
    '11': {
            "code": 11,
            "message": "Media Out"
    },
    '28': {
            "code": 28,
            "message": "Printhead Detection Error"
    },
    '24': {
            "code": 24,
            "message": "Bad Printhead Element"
    },
    '32': {
            "code": 32,
            "message": "Printhead Thermistor Open"
    },
    '31': {
            "code": 31,
            "message": "Invalid Firmware Config."
    },
    '48': {
            "code": 48,
            "message": "Clear Paper Path Failed"
    },
    '44': {
            "code": 44,
            "message": "Paper Feed Error"
    },
    '42': {
            "code": 42,
            "message": "Presenter Not Running"
     },
    '41': {
            "code": 41,
            "message": "Paper Jam during Retract"
    },
    '58': {
            "code": 58,
            "message": "Black Mark not Found"
    },
    '54': {
            "code": 54,
            "message": "Black Mark Calabrate Error"
    },
    '52': {
            "code": 52,
            "message": "Retract Function timed out"
    },
    '51': {
            "code": 51,
            "message": "Paused"
    }
}

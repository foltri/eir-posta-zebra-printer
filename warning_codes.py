codes = {
    '18': {
            "code": 18,
            "message": "Paper-near-end Sensor"
     },
    '14': {
            "code": 14,
            "message": "Replace Printhead"
         },
    '12': {
            "code": 12,
            "message": "Need to Calibrate Media"
    },
    '11': {
            "code": 11,
            "message": "Need to Calibrate Media"
         },
    '21': {
            "code": 21,
            "message": "Sensor 1 (Paper beforehead)"
    },
    '24': {
            "code": 24,
            "message": "Sensor 3 (Paper after head)"
         },
    '28': {
            "code": 28,
            "message": "Sensor 4 (loop ready)"
    },
    '31': {
            "code": 31,
            "message": "Sensor 5 (presenter)"
         },
    '32': {
            "code": 32,
            "message": "Sensor 6 (retract ready)"
    },
    '34': {
            "code": 34,
            "message": "Sensor 7 (in retract)"
         },
    '38': {
            "code": 38,
            "message": "Sensor 8 (at bin)"
    }
}

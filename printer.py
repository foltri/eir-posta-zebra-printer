import zebra
import zpl_generator

debug = True


def print_label(id, name, date_arrival, date_expiry, teteles, tag_id=None):
    zpl_data = zpl_generator.generatezpl(id, name, date_arrival, date_expiry, teteles, tag_id=tag_id, debug=debug)
    errors = zebra.print_zpl(zpl_data)
    print(errors)


def print_uvk_sticker(uvk_cause_id, postal_code):
    zpl_data = zpl_generator.generate_uvk_zpl(uvk_cause_id, postal_code, debug=debug)
    errors = zebra.print_zpl(zpl_data)


if __name__ == '__main__':
    print_label(23045, "Kökörcsin", '08.27', '09.01', teteles=False, tag_id='12345')
    # print_uvk_sticker(1, 1084)